<?php include 'common/header.html' ?>

	<section class="fix"></section>

	<section class="faq faq-width">
		<div class="heading">
			<h2>Frequently Asked Questions</h2>
			<hr class="separator">
			<div class="sub-heading">
				We can help you te get the best financial result based on long-term success.<br>
				We advise you to choose one of our offer.
			</div>
		</div>

		<div class="faq_container">
			<ul>
				<li class="faq_item">
					<a class="faq_title"><span class="left">What is the cost of consult?</span></a>
					<div class="faq_content">
						<div class="text-component">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae quidem blanditiis delectus corporis, possimus officia sint sequi ex tenetur id impedit est pariatur iure animi non a ratione reiciendis nihil sed consequatur atque repellendus fugit perspiciatis rerum et. Dolorum consequuntur fugit deleniti, soluta fuga nobis. Ducimus blanditiis velit sit iste delectus obcaecati debitis omnis, assumenda accusamus cumque perferendis eos aut quidem! Aut, totam rerum, cupiditate quae aperiam voluptas rem inventore quas, ex maxime culpa nam soluta labore at amet nihil laborum? Explicabo numquam, sit fugit, voluptatem autem atque quis quam voluptate fugiat earum rem hic, reprehenderit quaerat tempore at. Aperiam.</p>
						</div>
					</div>
				</li>
				<li class="faq_item">
					<a class="faq_title"><span class="left">How do we work?</span></a>
					<div class="faq_content">
						<div class="text-component">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae quidem blanditiis delectus corporis, possimus officia sint sequi ex tenetur id impedit est pariatur iure animi non a ratione reiciendis nihil sed consequatur atque repellendus fugit perspiciatis rerum et. Dolorum consequuntur fugit deleniti, soluta fuga nobis. Ducimus blanditiis velit sit iste delectus obcaecati debitis omnis, assumenda accusamus cumque perferendis eos aut quidem! Aut, totam rerum, cupiditate quae aperiam voluptas rem inventore quas, ex maxime culpa nam soluta labore at amet nihil laborum? Explicabo numquam, sit fugit, voluptatem autem atque quis quam voluptate fugiat earum rem hic, reprehenderit quaerat tempore at. Aperiam.</p>
						</div>
					</div>
				</li>
				<li class="faq_item">
					<a class="faq_title"><span class="left">Lorem ipsum?</span></a>
					<div class="faq_content">
						<div class="text-component">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae quidem blanditiis delectus corporis, possimus officia sint sequi ex tenetur id impedit est pariatur iure animi non a ratione reiciendis nihil sed consequatur atque repellendus fugit perspiciatis rerum et. Dolorum consequuntur fugit deleniti, soluta fuga nobis. Ducimus blanditiis velit sit iste delectus obcaecati debitis omnis, assumenda accusamus cumque perferendis eos aut quidem! Aut, totam rerum, cupiditate quae aperiam voluptas rem inventore quas, ex maxime culpa nam soluta labore at amet nihil laborum? Explicabo numquam, sit fugit, voluptatem autem atque quis quam voluptate fugiat earum rem hic, reprehenderit quaerat tempore at. Aperiam.</p>
						</div>
					</div>
				</li>
				<li class="faq_item">
					<a class="faq_title"><span class="left">Lorem ipsum?</span></a>
					<div class="faq_content">
						<div class="text-component">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae quidem blanditiis delectus corporis, possimus officia sint sequi ex tenetur id impedit est pariatur iure animi non a ratione reiciendis nihil sed consequatur atque repellendus fugit perspiciatis rerum et. Dolorum consequuntur fugit deleniti, soluta fuga nobis. Ducimus blanditiis velit sit iste delectus obcaecati debitis omnis, assumenda accusamus cumque perferendis eos aut quidem! Aut, totam rerum, cupiditate quae aperiam voluptas rem inventore quas, ex maxime culpa nam soluta labore at amet nihil laborum? Explicabo numquam, sit fugit, voluptatem autem atque quis quam voluptate fugiat earum rem hic, reprehenderit quaerat tempore at. Aperiam.</p>
						</div>
					</div>
				</li>
				<li class="faq_item">
					<a class="faq_title"><span class="left">Lorem ipsum?</span></a>
					<div class="faq_content">
						<div class="text-component">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae quidem blanditiis delectus corporis, possimus officia sint sequi ex tenetur id impedit est pariatur iure animi non a ratione reiciendis nihil sed consequatur atque repellendus fugit perspiciatis rerum et. Dolorum consequuntur fugit deleniti, soluta fuga nobis. Ducimus blanditiis velit sit iste delectus obcaecati debitis omnis, assumenda accusamus cumque perferendis eos aut quidem! Aut, totam rerum, cupiditate quae aperiam voluptas rem inventore quas, ex maxime culpa nam soluta labore at amet nihil laborum? Explicabo numquam, sit fugit, voluptatem autem atque quis quam voluptate fugiat earum rem hic, reprehenderit quaerat tempore at. Aperiam.</p>
						</div>
					</div>
				</li>
				<li class="faq_item">
					<a class="faq_title"><span class="left">Lorem ipsum?</span></a>
					<div class="faq_content">
						<div class="text-component">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae quidem blanditiis delectus corporis, possimus officia sint sequi ex tenetur id impedit est pariatur iure animi non a ratione reiciendis nihil sed consequatur atque repellendus fugit perspiciatis rerum et. Dolorum consequuntur fugit deleniti, soluta fuga nobis. Ducimus blanditiis velit sit iste delectus obcaecati debitis omnis, assumenda accusamus cumque perferendis eos aut quidem! Aut, totam rerum, cupiditate quae aperiam voluptas rem inventore quas, ex maxime culpa nam soluta labore at amet nihil laborum? Explicabo numquam, sit fugit, voluptatem autem atque quis quam voluptate fugiat earum rem hic, reprehenderit quaerat tempore at. Aperiam.</p>
						</div>
					</div>
				</li>
			</ul>

	</section>
	<script src="js/accordion.js"></script>

	<section class="row-contact full-width">
        <div class="centered-width">
            <div class="row">
                <img class="left question-icon" src="img/icons/question-icon.png" alt="question-mark">
                <div class="left question-tag">
                    <div class="question1">Have any question?</div>
                    <div class="question2">We can help you. Call us or send us an email.</div>
                </div>
                <a  class="right button" href="contact.php">Get in touch</a>
            </div>
        </div>
    </section>

<?php include 'common/footer.html' ?>