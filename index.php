<?php include 'common/header.html'?>

    <section class="fix-top"></section>

    <section class="cd-hero js-cd-hero js-cd-autoplay">
        <ul class="cd-hero__slider">
           <li class="cd-hero__slide cd-hero__slide--selected js-cd-slide">
                <div class="cd-hero__content cd-hero__content--full-width" style="background:url(img/sh1446278705.jpg) no-repeat 50% 50%;background-size:cover">
                    <h2>FINANCE ANALYSIS</h2>
                    <p>We can help you, make the best decision!</p>
                    <a href="#offer" class="cd-hero__btn">Learn More</a>
                </div> <!-- .cd-hero__content -->
           </li>
           <li class="cd-hero__slide cd-hero__slide--selected js-cd-slide">
                <div class="cd-hero__content cd-hero__content--full-width" style="background:url(img/GI921054562.png) no-repeat 50% 50%;background-size:cover">
                    <h2>FINANCE ANALYSIS</h2>
                    <p>We can help you, make the best decision!</p>
                    <a href="#offer" class="cd-hero__btn">Learn More</a>
                </div> <!-- .cd-hero__content -->
           </li>
        </ul> <!-- .cd-hero__slider -->
    </section>



    <section class="our-offer centered-width" id="offer">
        <div class="heading">
            <h2>OUR OFFER</h2>
            <hr class="separator">
            <div class="sub-heading">
                We can help you te get the best financial result based on long-term success.<br>
                We advise you to choose one of our offer.
            </div>
        </div>
        <div class="offer-container">
            <div class="offer-item left">
                <img src="img/vectors/vector1.png" alt="investment-loan">
                <div class="item-header">Investment Loan</div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus rhoncus tellus eget tellus elementum, elementum venenatis massa tempus. Sed eu erat ut quam dignissim ultricies sed nec lacus. </p>
            </div>
            <div class="offer-item left">
                <img src="img/vectors/vector2.png" alt="investment-loan">
                <div class="item-header">Real Estete & Planning</div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus rhoncus tellus eget tellus elementum, elementum venenatis massa tempus. Sed eu erat ut quam dignissim ultricies sed nec lacus. </p>
            </div>
            <div class="offer-item left">
                <img src="img/vectors/vector3.png" alt="investment-loan">
                <div class="item-header">Investment Management</div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus rhoncus tellus eget tellus elementum, elementum venenatis massa tempus. Sed eu erat ut quam dignissim ultricies sed nec lacus. </p>
            </div>
            <div class="offer-item left">
                <img src="img/vectors/vector1.png" alt="investment-loan">
                <div class="item-header">Investment Loan</div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus rhoncus tellus eget tellus elementum, elementum venenatis massa tempus. Sed eu erat ut quam dignissim ultricies sed nec lacus. </p>
            </div>
            <div class="offer-item left">
                <img src="img/vectors/vector2.png" alt="investment-loan">
                <div class="item-header">Real Estete & Planning</div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus rhoncus tellus eget tellus elementum, elementum venenatis massa tempus. Sed eu erat ut quam dignissim ultricies sed nec lacus. </p>
            </div>
            <div class="offer-item left">
                <img src="img/vectors/vector3.png" alt="investment-loan">
                <div class="item-header">Investment Management</div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus rhoncus tellus eget tellus elementum, elementum venenatis massa tempus. Sed eu erat ut quam dignissim ultricies sed nec lacus. </p>
            </div>
        </div>
    </section>

    <section class="counterup full-width">
        <div class="row centered-width">
            <div class="counter-con">
                <i class="counter-icon fa fa-briefcase" aria-hidden="true"></i>
                <span class="counter">23</span>
                <span class="counter-title">years in finance </span>
            </div>
            <div class="counter-con">
                <i class="counter-icon fa fa-smile-o" aria-hidden="true"></i>
                <span class="counter">2567</span>
                <span class="counter-title">satisfied customers</span>
            </div>
            <div class="counter-con">
                <i class="counter-icon fa fa-coffee" aria-hidden="true"></i>
                <span class="counter">18653</span>
                <span class="counter-title">cups of coffe</span>
            </div>
            <div class="counter-con">
                <i class="counter-icon fa fa-btc" aria-hidden="true"></i>
                <span class="counter">8,231,312</span>
                <span class="counter-title">stock exchanges</span>
            </div>
            <script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
            <script src="js/addons/jquery.counterup.min.js"></script>
        </div>
        <script>
            jQuery(document).ready(function($) {
                $('.counter').counterUp({
                    delay: 10,
                    time: 2000
                });
            });
        </script>
    </section>
    
    
    

    <section id="about" class="experts full-width">
            <div class="left img-container"></div>
            <div class="half-centered-width left">
                <div class="column-container">
                    <h2>Financial Experts</h2>
                    <h3>providing the best financial consultants</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus rhoncus tellus eget tellus elementum, elementum venenatis massa tempus. Sed eu erat ut quam dignissim ultricies sed nec lacus. Phasellus porta porta urna, id efficitur purus sollicitudin at. Mauris euismod vehicula fermentum. Integer ornare tellus felis. Fusce in lectus erat. Curabitur sit amet semper sapien, eu iaculis eros. Praesent sagittis finibus arcu nec dapibus. Aenean at risus porttitor mauris blandit congue.</p>
                </div>
            </div>
    </section>

    <section class="row-contact full-width">
        <div class="centered-width">
            <div class="row">
                <img class="left question-icon" src="img/icons/question-icon.png" alt="question-mark">
                <div class="left question-tag">
                    <div class="question1">Have any question?</div>
                    <div class="question2">We can help you. Call us or send us an email.</div>
                </div>
                <a  class="right button" href="contact.php">Get in touch</a>
            </div>
        </div>
    </section>

<?php include 'common/footer.html' ?>